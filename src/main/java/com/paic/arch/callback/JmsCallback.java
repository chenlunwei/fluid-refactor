package com.paic.arch.callback;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;

/**
 * 把回调接口类单独抽出来
 * Created by chenlunwei on 2018/3/8.
 */
public interface JmsCallback {
	String performJmsFunction(Session aSession, Destination aDestination) throws JMSException;

}
