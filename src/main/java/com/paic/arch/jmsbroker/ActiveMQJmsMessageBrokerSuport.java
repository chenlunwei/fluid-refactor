package com.paic.arch.jmsbroker;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;

import javax.jms.ConnectionFactory;

/**
 * JmsMessageBrokerSupport的实现类
 * 完成创建ConnectionFactory已经管理Broker生命周期的业务逻辑
 * Created by chenlunwei on 2018/3/8.
 */
public class ActiveMQJmsMessageBrokerSuport extends JmsMessageBrokerSupport {
	private BrokerService brokerService;

	public ActiveMQJmsMessageBrokerSuport() throws Exception {
		super();
	}

	@Override
	public ActiveMQJmsMessageBrokerSuport bindToBrokerAtUrl(String aBrokerUrl) throws Exception {
		return new ActiveMQJmsMessageBrokerSuport();
	}

	@Override
	public void createAndStartEmbeddedBroker() throws Exception {
		createEmbeddedBroker();
		startEmbeddedBroker();
	}

	private void createEmbeddedBroker() throws Exception {
		brokerService = new BrokerService();
		brokerService.setPersistent(false);
		brokerService.addConnector(super.getBrokerUrl());
	}

	private void startEmbeddedBroker() throws Exception {
		brokerService.start();
	}

	@Override
	public void stopTheRunningBroker() throws Exception {
		if (brokerService == null) {
			throw new IllegalStateException("Cannot stop the broker from this API: " +
					"perhaps it was started independently from this utility");
		}
		brokerService.stop();
		brokerService.waitUntilStopped();
	}

	@Override
	public ConnectionFactory createConnectionFactory() {
		System.out.println("active");
		return new ActiveMQConnectionFactory(super.getBrokerUrl());
	}

	@Override
	public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
		Broker regionBroker = brokerService.getRegionBroker();
		for (org.apache.activemq.broker.region.Destination destination : regionBroker.getDestinationMap().values()) {
			if (destination.getName().equals(aDestinationName)) {
				return destination.getDestinationStatistics().getMessages().getCount();
			}
		}
		throw new IllegalStateException(String.format("Destination %s does not exist on broker at %s", aDestinationName, super.getBrokerUrl()));
	}
}
