package com.paic.arch.jmsbroker;

import com.paic.arch.callback.JmsCallback;
import com.paic.arch.exception.NoMessageReceivedException;
import org.slf4j.Logger;

import javax.jms.*;
import java.lang.IllegalStateException;

import static com.paic.arch.jmsbroker.SocketFinder.findNextAvailablePortBetween;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * 把和具体框架(activeMQ)相关的业务抽离出来
 * 把JmsMessageBrokerSupport作为一个抽象类
 * 具体的创建ConnectionFactory和管理Broker的生命周期的业务由其子类实现
 */
public abstract class JmsMessageBrokerSupport{
	private static final Logger LOG = getLogger(JmsMessageBrokerSupport.class);
	private static final int ONE_SECOND = 1000;
	private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;
	public static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";
	private static String brokerUrl;
	private ConnectionFactory connectionFactory;

	static {
		//创建JmsMessageBrokerSupport对象时，初始化brokerUrl参数
		brokerUrl = DEFAULT_BROKER_URL_PREFIX + findNextAvailablePortBetween(41616, 50000);
	}

	public void setConnectionFactory() {
		this.connectionFactory = createConnectionFactory();
	}


	public JmsMessageBrokerSupport() throws Exception {
		setConnectionFactory();
	}

	public final JmsMessageBrokerSupport andThen() {
		return this;
	}

	public abstract JmsMessageBrokerSupport bindToBrokerAtUrl(String aBrokerUrl) throws Exception;

	public JmsMessageBrokerSupport createARunningEmbeddedBrokerOnAvailablePort() throws Exception {
		return createARunningEmbeddedBrokerAt(brokerUrl);
	}


	public JmsMessageBrokerSupport createARunningEmbeddedBrokerAt(String aBrokerUrl) throws Exception {
		LOG.debug("Creating a new broker at {}", aBrokerUrl);
		JmsMessageBrokerSupport broker = bindToBrokerAtUrl(aBrokerUrl);
		broker.createAndStartEmbeddedBroker();
		return broker;
	}

	//创建和开启Broker的方法，具体业务逻辑由其子类实现
	public abstract void createAndStartEmbeddedBroker() throws Exception;
	//停止Broker的方法，具体业务逻辑由其子类实现
	public abstract void stopTheRunningBroker() throws Exception;
	//创建ConnectionFactory的方法，具体业务逻辑由其子类实现
	public abstract ConnectionFactory createConnectionFactory();
	//获取队列里消息数量的方法，具体业务逻辑由其子类实现
	public abstract long getEnqueuedMessageCountAt(String aDestinationName) throws Exception;

	public final String getBrokerUrl() {
		return brokerUrl;
	}

	public JmsMessageBrokerSupport sendATextMessageToDestinationAt(String aDestinationName, final String aMessageToSend) {
		executeCallbackAgainstRemoteBroker(brokerUrl, aDestinationName, (aSession, aDestination) -> {
			MessageProducer producer = aSession.createProducer(aDestination);
			producer.send(aSession.createTextMessage(aMessageToSend));
			producer.close();
			return "";
		});
		return this;
	}

	public String retrieveASingleMessageFromTheDestination(String aDestinationName) {
		return retrieveASingleMessageFromTheDestination(aDestinationName, DEFAULT_RECEIVE_TIMEOUT);
	}

	public String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout) {
		return executeCallbackAgainstRemoteBroker(brokerUrl, aDestinationName, (aSession, aDestination) -> {
			MessageConsumer consumer = aSession.createConsumer(aDestination);
			Message message = consumer.receive(aTimeout);
			if (message == null) {
				throw new NoMessageReceivedException(String.format("No messages received from the broker within the %d timeout", aTimeout));
			}
			consumer.close();
			return ((TextMessage) message).getText();
		});
	}

	private String executeCallbackAgainstRemoteBroker(String aBrokerUrl, String aDestinationName, JmsCallback aCallback) {
		Connection connection = null;
		String returnValue = "";
		try {
			connection = connectionFactory.createConnection();
			connection.start();
			returnValue = executeCallbackAgainstConnection(connection, aDestinationName, aCallback);
		} catch (JMSException jmse) {
			LOG.error("failed to create connection to {}", aBrokerUrl);
			throw new IllegalStateException(jmse);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (JMSException jmse) {
					LOG.warn("Failed to close connection to broker at []", aBrokerUrl);
					throw new IllegalStateException(jmse);
				}
			}
		}
		return returnValue;
	}


	private String executeCallbackAgainstConnection(Connection aConnection, String aDestinationName, JmsCallback aCallback) {
		Session session = null;
		try {
			session = aConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Queue queue = session.createQueue(aDestinationName);
			return aCallback.performJmsFunction(session, queue);
		} catch (JMSException jmse) {
			LOG.error("Failed to create session on connection {}", aConnection);
			throw new IllegalStateException(jmse);
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (JMSException jmse) {
					LOG.warn("Failed to close session {}", session);
					throw new IllegalStateException(jmse);
				}
			}
		}
	}

	public boolean isEmptyQueueAt(String aDestinationName) throws Exception {
		return getEnqueuedMessageCountAt(aDestinationName) == 0;
	}

}
