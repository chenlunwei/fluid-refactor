package com.paic.arch.exception;

/**
 * 把异常类单独抽出来
 * Created by chenlunwei on 2018/3/8.
 */
public class NoMessageReceivedException extends RuntimeException {
	public NoMessageReceivedException(String reason) {
		super(reason);
	}
}
